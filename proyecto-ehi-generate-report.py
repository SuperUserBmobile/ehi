import os
import json
import email
import boto3
import base64
import pyodbc
import pandas as pd
import datetime as dt

s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')
s3_bucket = s3_resource.Bucket('proyecto-ehi-correos')
dynamodb_client = boto3.client('dynamodb')
dynamodb_table = 'proyecto-ehi-dynamodb'
dynamodb_waiter_drop = dynamodb_client.get_waiter('table_not_exists')

server = os.environ["SQL_SERVER"]
database = os.environ["SQL_DATABASE"]
username = os.environ["SQL_USERNAME"]
password = os.environ["SQL_PASSWORD"]

today = dt.datetime.today()

def lambda_handler(event, context):

    dynamo_df = get_data_dynamo()
    dynamo_df = dynamo_df.rename(columns = {'asset': 'Asset_AssetTag'})
    dynamo_df.set_index('Asset_AssetTag', inplace=True)
    dynamo_df['start_date'] = dynamo_df['start_date'].astype('datetime64')
    dynamo_df['end_date'] = dynamo_df['end_date'].astype('datetime64')

    excel_key = list_reports_excel()
    excel_df = get_data_excel(excel_key)
    
    vivantio_df = get_data_vivantio()
    vivantio_df = vivantio_df.rename(columns={'Ehi_PaymentLongSerialNumber': 'Serial number'})
    
    merged_df = excel_df.merge(vivantio_df, how = 'inner', on = ['Serial number'])
    merged_df['Terminal signature'] = merged_df['Terminal signature'].astype("Int64").astype(str)
    merged_df['Last call start date'] = merged_df['Last call start date'].astype('datetime64')
    merged_df['Last call end date'] = merged_df['Last call end date'].astype('datetime64')
    merged_df = merged_df.rename(columns = {'Last call start date': 'start_date', 'Last call end date': 'end_date', 'Last call status': 'status'})
    merged_df['status'] = merged_df['status'].apply( lambda x : "FAILURE")
    merged_df.set_index('Asset_AssetTag', inplace=True)
    merged_df.update(dynamo_df)

    report_df = clean_report(merged_df) 

    upload_report_to_s3(report_df)
    
    #renew_dynamodb_table()
    
    return {'statusCode': 200}
    
################################################################################

def get_data_dynamo():
    dynamodb_response = dynamodb_client.scan(
        TableName = dynamodb_table
    )
    dataframe = pd.DataFrame()
    json_subitems = {}
    for item in dynamodb_response['Items']:
        for subitem in item:
            json_subitems[subitem] = item[subitem]['S']
        dataframe = dataframe.append(json_subitems, ignore_index=True)
    return dataframe
    
def list_reports_excel():
    s3_response = s3_client.list_objects_v2(
        Bucket = "proyecto-ehi-correos"
    )
    files_s3 = s3_response['Contents']
    files_s3_df = pd.DataFrame(files_s3)
    files_s3_df = files_s3_df.sort_values('LastModified', ascending = False)
    new_df = files_s3_df.loc[files_s3_df['Key'].str.startswith('inbound')]
    #print("RESULTADOS PARA CARPETA INBOUND:")
    #for row in new_df.iterrows():
    #    print(row[1]['Key'], row[1]['LastModified'])
    #print("")
    return new_df.iloc[0]['Key']

def get_data_excel(s3_key):
    bucket = "proyecto-ehi-correos"
    waiter = s3_client.get_waiter('object_exists')
    waiter.wait( Bucket = bucket, Key = s3_key)
    response = s3_resource.Bucket(bucket).Object(s3_key)
    msg = response.get()["Body"].read().decode("utf-8")
    msg = email.message_from_string(msg)
    attachment = msg.get_payload()[1]
    payload = attachment.get_payload(decode=True)
    open('/tmp/attachment.xlsx', 'wb').write(payload)
    return pd.read_excel("/tmp/attachment.xlsx")
    
def get_data_vivantio():
    conn_string = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password +';TrustServerCertificate=no;'
    query_string = "SELECT * FROM vW_L3KAssetReport;"
    conn = pyodbc.connect(conn_string)
    return pd.read_sql(query_string, conn)
    
def clean_report(dataframe):
    dataframe = dataframe.rename(
        columns = {
            "Asset_AssetTag": 'Asset_Tag',
            "Location_Country": 'Country',
            "Estate signature": "Estate_Signature",
            "Estate name": "Estate_Name", 
            "Part number": "Part_Number",
            "Terminal name": "Terminal_Name",
            "Terminal signature": "Terminal_Signature", 
            "Serial number": "Serial_Number", 
            "start_date": 'Last_Call_Start_Date',
            "end_date": 'Last_Call_End_Date',
            "status": 'Last_Call_Status',
            "Asset_CategoryLineage": "Asset_Category"
        }
    )
    dataframe['Last_Call_Duration'] = pd.to_timedelta(dataframe['Last_Call_End_Date'] - dataframe['Last_Call_Start_Date'], unit = 's').astype(str).apply( lambda x: x[7:15] )
    #print(dataframe['Last_Call_Duration'])
    dataframe["Location_Phone"] = dataframe["Location_Phone"].str.replace(' ', '').str.replace('/', '').str.replace('+', '').str.replace('-', '').str.replace('(', '').str.replace(')', '')
    dataframe.loc[dataframe['Country'] == "FRA", 'Lada'] = "+33"
    dataframe.loc[dataframe['Country'] == "ES", 'Lada'] = "+34"
    dataframe.loc[dataframe['Country'] == "UK", 'Lada'] = "+44"
    dataframe.loc[dataframe['Country'] == "DE", 'Lada'] = "+49"
    dataframe.loc[dataframe['Country'] == "IE", 'Lada'] = "+353"
    dataframe = dataframe[[
        #"Asset_Tag",
        "Country",
        "Estate_Signature",
        "Estate_Name", 
        "Location_Name",
        "Lada",
        "Location_Phone",
        "Part_Number",
        "Terminal_Name",
        "Terminal_Signature", 
        "Serial_Number",
        "Last_Call_Start_Date", 
        "Last_Call_End_Date", 
        "Last_Call_Duration",
        "Last_Call_Status", 
        "Asset_Category"
    ]]
    dataframe.loc[(dataframe['Last_Call_End_Date'] == "") | (dataframe['Last_Call_End_Date'].isna()), 'Last_Call_Status'] = "FAILURE"
    dataframe.loc[(dataframe['Location_Phone'] == "") | (dataframe['Location_Phone'].isna()), 'Last_Call_Status'] = "NO PHONE"
    dataframe.loc[
        (dataframe["Estate_Name"] == "ESP") | 
        (dataframe["Estate_Name"] == "FRA") | 
        (dataframe["Estate_Name"] == "GER") |
        (dataframe["Estate_Name"] == "UK"),
        "Last_Call_Status"
    ] = "NOT ASSIGNED"
    return dataframe

def upload_report_to_s3(dataframe):
    print("###Subiendo reporte")
    bucket = "proyecto-ehi-correos"
    key = "reports/" + today.strftime("%Y-%m-%d %H:%M:%S") + ".xlsx"
    dataframe.to_excel('/tmp/Reporte_demo.xlsx', encoding='utf-8', index = True)
    with open('/tmp/Reporte_demo.xlsx', "rb") as file:
        s3_client.upload_fileobj(file, bucket, key)
    print("#Reporte subido con exito")

def renew_dynamodb_table():
    dynamodb_response = dynamodb_client.delete_table(
        TableName = dynamodb_table
    )
    print("### SE ELIMINO LA TABLA CON EXITO ###")

    dynamodb_waiter_drop.wait(
        TableName = dynamodb_table,
        WaiterConfig = {
            'Delay': 1,
            'MaxAttempts': 10
        }
    )
    
    dynamodb_response = dynamodb_client.create_table(
        AttributeDefinitions = [
            {
                'AttributeName': 'asset',
                'AttributeType': 'S'
            }
        ],
        TableName = dynamodb_table,
        KeySchema = [
            {
                'AttributeName': 'asset',
                'KeyType': 'HASH'
            }
        ],
        BillingMode = 'PAY_PER_REQUEST'
    )
    
    print("### SE CREO LA TABLA CON EXITO ###")