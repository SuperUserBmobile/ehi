import os
import json
import email
import boto3
import base64
import pyodbc
import pandas as pd
import datetime as dt
import requests
from requests.auth import HTTPBasicAuth
from email import encoders
from email.mime.base import MIMEBase
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')
ses_client = boto3.client('sesv2')
ses_client_v1 = boto3.client('ses')
connect_client = boto3.client('connect')
dynamodb_client = boto3.client('dynamodb')
dynamodb_table = 'proyecto-ehi-closed-branches'

max_days = int(os.environ["AWAIT_DAYS"])
max_calls = int(os.environ["MAX_CALLS"])
server = os.environ["SQL_SERVER"]
database = os.environ["SQL_DATABASE"]
username = os.environ["SQL_USERNAME"]
password = os.environ["SQL_PASSWORD"]
vivantio_username = os.environ["VIVANTIO_USERNAME"]
vivantio_password = os.environ["VIVANTIO_PASSWORD"]

email_fra = os.environ["EMAIL_FRA"]
email_es = os.environ["EMAIL_ES"]
email_uk = os.environ["EMAIL_UK"]
email_de = os.environ["EMAIL_DE"]
email_ie = os.environ["EMAIL_IE"]
################################## INICIAN CORREOS DE PRUEBA
#email_fra = os.environ["EMAIL_IE"]
#email_es = os.environ["EMAIL_IE"]
#email_uk = os.environ["EMAIL_IE"]
#email_de = os.environ["EMAIL_IE"]
################################## TERMINAN CORREOS DE PRUEBA

connect_instance = os.environ["CONNECT_INSTANCE"]
connect_source_phone_FRA = os.environ["CONNECT_PHONE_FRA"]
connect_source_phone_ES = os.environ["CONNECT_PHONE_ES"]
connect_source_phone_UK = os.environ["CONNECT_PHONE_UK"]
connect_source_phone_DE = os.environ["CONNECT_PHONE_DE"]
connect_source_phone_IE = os.environ["CONNECT_PHONE_IE"]
connect_flow_FRA = os.environ["CONNECT_FLOW_FRA"]
connect_flow_ES = os.environ["CONNECT_FLOW_ES"]
connect_flow_UK = os.environ["CONNECT_FLOW_UK"]
connect_flow_DE = os.environ["CONNECT_FLOW_DE"]
connect_flow_IE = os.environ["CONNECT_FLOW_IE"]

today_date = dt.date.today()
limit_date = today_date - dt.timedelta(days = max_days)

def lambda_handler(event, context):
    
    vivantio_df = get_data_vivantio()
    vivantio_df = vivantio_df.rename(columns={'Ehi_PaymentLongSerialNumber': 'Serial number'})
    print(vivantio_df)
    excel_df = get_data_excel(event)

    sub_excel_df = excel_df[["Estate name","Serial number", "Last call end date", "Last call status"]]
    filtered_df = filter_data_excel(sub_excel_df)
    merged_df = filtered_df.merge(vivantio_df, how = 'inner', on = ['Serial number'])
    merged_df = merged_df[["Estate name", "Location_Country", "Location_Name", "Serial number", "Asset_AssetTag", "Location_Phone", "Last call end date"]]
    merged_df = merged_df.sort_values("Estate name", ascending = True)
    #print("Hay un total de", excel_df.shape[0], "assets en el excel.")
    #print("")
    #for row in final_df.iterrows():
    #    print(row)
    #print("")
    
    print("LA FECHA DEL REPORTE ES", today_date)
    
    cleaned_df = clean_data(merged_df)
    #print(cleaned_df)
    cleaned_df = add_lada(cleaned_df)
    cleaned_df = filter_closed_branches(cleaned_df)
    final_df = filter_disabled_reports(cleaned_df)
    disable_notifications(final_df)
    prepare_email(final_df)
    # EXAMPLE: [Estate name, Location_Country, Serial number, Asset_AssetTag, Location_Phone, Last call end date, Location_Lada]
    
    print("Hay un total de", final_df.shape[0], "assets, cuya última llamada fue antes del", limit_date)
    print("")
    groups_by_clients = final_df.groupby(["Location_Phone"])
    total_clients = len(groups_by_clients)
    
    report_df = excel_df.merge(vivantio_df, how = 'outer', on = ['Serial number'])
    report_df['Terminal signature'] = report_df['Terminal signature'].astype("Int64").astype(str)
    report_df['Last call start date'] = report_df['Last call start date'].astype('datetime64')
    report_df['Last call end date'] = report_df['Last call end date'].astype('datetime64')
    report_df = report_df.rename(columns = {'Last call start date': 'start_date', 'Last call end date': 'end_date', 'Last call status': 'status'})
    report_df['status'] = report_df['status'].apply( lambda x : "")
    report_df = clean_report(report_df)
    report_df['Location_Phone'] = report_df['Location_Phone'].astype(str)
    report_df['Location_Phone'] = report_df['Location_Phone'].apply( lambda x : "'" + x).astype(str)
    report_df['Serial_Number'] = report_df['Serial_Number'].astype(str)
    report_df['Serial_Number'] = report_df['Serial_Number'].apply( lambda x : "'" + x).astype(str)
    report_df.set_index('Asset_Tag', inplace=True)
    filename = "Reporte " + today_date.strftime("%Y-%m-%d") + ".csv"
    report_df.to_csv(
        '/tmp/' + filename, 
        encoding='utf-8', 
        index = True
    )
    report_analytics = analyze_dataframe(report_df)
    send_report_global(report_analytics, filename)
    
    #assets_by_phone = final_df.groupby(["Location_Lada", "Location_Phone"])[["Asset_AssetTag"]].apply(lambda g: g.values.tolist()).to_dict()
    #print(assets_by_phone)
    #assets_by_client = final_df.groupby(["Estate name"])[["Asset_AssetTag", "Location_Phone"]].apply(lambda g: g.values.tolist()).to_dict()
    #assets_by_client = final_df.groupby(["Estate name"]).apply(lambda g: g.values.tolist()).to_dict()
    #print(assets_by_client)
    
    ### INICIAN DATOS PARA PRUEBAS
    #total_clients = 3
    #assets_by_phone = {('+52', '5529901442'): [['01348354'], ['01348353'], ['01348355']]}
    #assets_by_phone = {('+52', '9992745320'): [['01348351'], ['01348352'], ['01348353']], ('+52', '9626259557'): [['01348354'], ['01348355'], ['01348356']], ('+52', '5529901442'): [['01348357'], ['01348358'], ['01348359']]}
    ### TERMINAN DATOS PARA PRUEBAS

    if total_clients > max_calls:
        print("Demasiadas llamadas, el maximo de llamadas permitidas es de:", max_calls, "y se están intentando realizar:", total_clients)
        print("FALTA AGREGAR EL ENVIO DE ALERTAS POR MAXIMO DE LLAMADAS")
    else:
        print("Realizando llamadas a", total_clients, "numeros...")
        print("")
        #prepare_call(assets_by_phone)
        #prepare_email(assets_by_client)
    print("")

##### FUNCIONES #####

def get_data_vivantio():
    conn_string = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password +';TrustServerCertificate=no;'
    query_string = "SELECT * FROM vW_L3KAssetReport;"
    conn = pyodbc.connect(conn_string)
    return pd.read_sql(query_string, conn)
    
def get_data_excel(event):
    bucket = "proyecto-ehi-correos"
    key = event['Records'][0]['ses']['mail']['messageId']
    key = "inbound/" + key
    waiter = s3_client.get_waiter('object_exists')
    waiter.wait( Bucket = bucket, Key = key)
    response = s3_resource.Bucket(bucket).Object(key)
    msg = response.get()["Body"].read().decode("utf-8")
    msg = email.message_from_string(msg)
    attachment = msg.get_payload()[1]
    payload = attachment.get_payload(decode=True)
    open('/tmp/attachment.xlsx', 'wb').write(payload)
    return pd.read_excel("/tmp/attachment.xlsx")
    
def filter_data_excel(new_df):
    filter_by_estate_name = new_df[(new_df["Estate name"] != "ESP") & (new_df["Estate name"] != "FRA") & (new_df["Estate name"] != "UK") & (new_df["Estate name"] != "GER")]
    filter_by_date = filter_by_estate_name[( pd.to_datetime(filter_by_estate_name["Last call end date"]) <= str(limit_date) )]
    return filter_by_date.sort_values("Last call end date", ascending = False)
    
def clean_data(df_for_clean):
    df_for_clean["Location_Phone"] = df_for_clean["Location_Phone"].str.replace(' ', '').str.replace('/', '').str.replace('-', '').str.replace('(', '').str.replace(')', '')
    #df_for_clean = df_for_clean[df_for_clean["Location_Phone"] != '']
    #return df_for_clean.dropna()
    return df_for_clean

def add_lada(df):
    df.loc[df['Location_Country'] == "FRA", 'Location_Lada'] = "+33"
    df.loc[df['Location_Country'] == "ES", 'Location_Lada'] = "+34"
    df.loc[df['Location_Country'] == "UK", 'Location_Lada'] = "+44"
    df.loc[df['Location_Country'] == "DE", 'Location_Lada'] = "+49"
    df.loc[df['Location_Country'] == "IE", 'Location_Lada'] = "+353"
    return df
    
def filter_disabled_reports(df):
    dynamodb_response = dynamodb_client.scan(
        TableName = "proyecto-ehi-last-report"
    )
    dynamodb_df = pd.DataFrame()
    print(dynamodb_df)
    json_subitems = {}
    for item in dynamodb_response['Items']:
        for subitem in item:
            json_subitems[subitem] = item[subitem]['S']
        dynamodb_df = dynamodb_df.append(json_subitems, ignore_index=True)
    cond = df['Asset_AssetTag'].isin(dynamodb_df['asset'])
    df.drop(df[cond].index, inplace = True)
    return df
    
def filter_closed_branches(df):
    dynamodb_response = dynamodb_client.scan(
        TableName = dynamodb_table
    )
    dynamodb_df = pd.DataFrame()
    json_subitems = {}
    for item in dynamodb_response['Items']:
        for subitem in item:
            json_subitems[subitem] = item[subitem]['S']
        dynamodb_df = dynamodb_df.append(json_subitems, ignore_index=True)
    cond = df['Estate name'].isin(dynamodb_df['Branch Code'])
    df.drop(df[cond].index, inplace = True)
    return df

def prepare_call(assets_by_phone):
    for phone in assets_by_phone:
        asset_number = 0
        json_assets = {}
        for asset in assets_by_phone[phone]:
            asset_number += 1
            json_assets["asset_" + str(asset_number)] = asset[0]
        filter_call_by_lang(phone, json_assets)
    print("")
    
def filter_call_by_lang(phone, assets):
    #print("Phone:", phone[0]+phone[1])
    #print("Attributes:", assets)
    if phone[0] == "+33":
        start_call_connect_FRA(phone, assets)
    #elif phone[0] == "+52" or phone[0] == "+34":
    elif phone[0] == "+52":
        start_call_connect_ES(phone, assets)
    elif phone[0] == "+44" or phone[0] == "+353":
        start_call_connect_UK(phone, assets)
    elif phone[0] == "+49":
        start_call_connect_DE(phone, assets)
    else:
        print("No se pudo obtener la lada")
    print("")
    
def start_call_connect_FRA(phone, assets):
    print("Iniciando llamada en Francia")
    
def start_call_connect_ES(phone, assets):
    full_phone = phone[0]+phone[1]
    print("Iniciando llamada en España al numero:", full_phone)
    print("Instance ID:", connect_instance)
    print("Source Phone Number:", connect_source_phone_ES)
    print("Flow ID:", connect_flow_ES)
    
    #connect_response = connect_client.start_outbound_voice_contact(
    #    DestinationPhoneNumber = "+525529901442", 
    #    ContactFlowId = "5e240e73-60ef-4b8a-b4b4-1d430bd76d67",
    #    InstanceId = "77647bba-5303-4fca-8488-50b1c99dd150",
    #    SourcePhoneNumber = "+34911239751",
    #    Attributes = {
    #        'asset_1': '123456789',
    #        'asset_2': '123456789'
    #    }
    #)
    
    
    connect_response = connect_client.start_outbound_voice_contact(
        DestinationPhoneNumber = full_phone, 
        ContactFlowId = connect_flow_ES,
        InstanceId = connect_instance,
        SourcePhoneNumber = connect_source_phone_ES,
        Attributes = assets
    )
    
    
    #contact_id = connect_response["ContactId"]
    #attr = connect_client.get_contact_attributes(
    #    InstanceId = connect_instance,
    #    InitialContactId = contact_id
    #)
    #print("imprimiendo attributos")
    #print(attr)
    
    print("Llamada ejecutada.")
    
def start_call_connect_UK(phone, assets):
    print("Iniciando llamada en Reino Unido")
    
def start_call_connect_DE(phone, assets):
    print("Iniciando llamada en Alemania")
    
def start_call_connect_IE(phone, assets):
    print("Iniciando llamada en Irlanda")
    
def prepare_email(df):
    # [Estate name, Location_Country, Serial number, Asset_AssetTag, Location_Phone, Last call end date, Location_Lada]
    registros_fra = df.loc[df['Location_Country'] == "FRA"]
    registros_es = df.loc[df['Location_Country'] == "ES"]
    registros_uk = df.loc[df['Location_Country'] == "UK"]
    registros_de = df.loc[df['Location_Country'] == "DE"]
    registros_ie = df.loc[df['Location_Country'] == "IE"]
    ############################################################################
    if registros_fra.empty:
        print("No hay registros para Francia")
    else:
        estate_name_fra = registros_fra['Estate name'].unique()
        estate_name_fra_dict = {estate : pd.DataFrame() for estate in estate_name_fra}
        for key in estate_name_fra_dict.keys():
            estate_name_fra_dict[key] = registros_fra[:][registros_fra['Estate name'] == key]
            send_report_by_email(key, email_fra, estate_name_fra_dict[key])
            create_ticket_vivantio("OffshoreTech France", estate_name_fra_dict[key])
    ############################################################################
    if registros_es.empty:
        print("No hay registros para España")
    else:
        estate_name_es = registros_es['Estate name'].unique()
        estate_name_es_dict = {estate : pd.DataFrame() for estate in estate_name_es}
        for key in estate_name_es_dict.keys():
            estate_name_es_dict[key] = registros_es[:][registros_es['Estate name'] == key]
            send_report_by_email(key, email_es, estate_name_es_dict[key])
            create_ticket_vivantio("Offshore Tech", estate_name_es_dict[key])
    ############################################################################
    if registros_uk.empty:
        print("No hay registros para UK")
    else:
        estate_name_uk = registros_uk['Estate name'].unique()
        estate_name_uk_dict = {estate : pd.DataFrame() for estate in estate_name_uk}
        for key in estate_name_uk_dict.keys():
            estate_name_uk_dict[key] = registros_uk[:][registros_uk['Estate name'] == key]
            send_report_by_email(key, email_uk, estate_name_uk_dict[key])
            create_ticket_vivantio("Qolcom", estate_name_uk_dict[key])
    ############################################################################
    if registros_de.empty:
        print("No hay registros para Alemania")
    else:
        estate_name_de = registros_de['Estate name'].unique()
        estate_name_de_dict = {estate : pd.DataFrame() for estate in estate_name_de}
        for key in estate_name_de_dict.keys():
            estate_name_de_dict[key] = registros_de[:][registros_de['Estate name'] == key]
            send_report_by_email(key, email_de, estate_name_de_dict[key])
            create_ticket_vivantio("SCHIFFL", estate_name_de_dict[key])
    ############################################################################
    if registros_ie.empty:
        print("No hay registros para Irlanda")
    else:
        estate_name_ie = registros_ie['Estate name'].unique()
        estate_name_ie_dict = {estate : pd.DataFrame() for estate in estate_name_ie}
        for key in estate_name_ie_dict.keys():
            estate_name_ie_dict[key] = registros_ie[:][registros_ie['Estate name'] == key]
            send_report_by_email(key, email_ie, estate_name_ie_dict[key])
            create_ticket_vivantio("Qolcom", estate_name_ie_dict[key])
    
def send_report_by_email(estate_name, email_destination, dataframe):
    dataframe = dataframe.sort_values('Last call end date', ascending = False)
    dataframe = dataframe[['Estate name', 'Asset_AssetTag', 'Serial number', 'Location_Lada', 'Location_Phone', 'Last call end date']]
    dataframe.reset_index(drop=True, inplace=True)
    print(dataframe)
    
    mensaje1 = '<p>Hi Team,</p>'
    mensaje1 += '<p>The following devices have not checked in with the Ingenico TEM Portal during the last 48 hours:</p>'
    mensaje2 = '<p>Please call the Branch and ask if they are experiencing any issues with the devices.</p>'
    mensaje2 += '<p>If there are issues, proceed with troubleshooting and escalate if required. If no apparent issues, please ask the Branch to reboot their devices and run a Test Transaction.</p>'
    mensaje2 += '<p>Kind regards,</p>'
    mensaje2 += '<p>GEMA Operations</p>'
    
    ses_response = ses_client.send_email(
        FromEmailAddress= 'reports@thegema.com.mx',
        Destination={
            'ToAddresses': [
                email_destination
            ]
        },
        Content={
            'Simple': {
                'Subject': {
                    'Data': "EHI Lane 3K - "+ estate_name + " - Devices not checked in during last 48 hours"
                },
                'Body': {
                    'Html': {
                        'Data': mensaje1 + dataframe.to_html(index=False) + mensaje2
                    }
                }
            }
        }
    )
    
def create_ticket_vivantio(group, dataframe):
    #['Estate name', 'Location_Country', 'Serial number', 'Asset_AssetTag', 'Location_Phone', 'Last call end date', 'Location_Lada']
    asset_list = ""
    for index in dataframe.index.values.tolist():
        asset_list += "\nAsset Tag: #"
        asset_list += dataframe.loc[index]['Asset_AssetTag']
        asset_list += " | Serial Number: "
        asset_list += dataframe.loc[index]['Serial number']
        asset_list += " | Last Call: "
        asset_list += dataframe.loc[index]['Last call end date']
        asset_list += ";"
    #print(asset_list)
    phone = dataframe['Location_Phone'].unique()
    phone = str(phone[0])
    branch = dataframe['Estate name'].unique()
    branch = str(branch[0])
    location = dataframe['Location_Name'].unique()
    location = str(location[0])
    title = "EHI Lane 3K - " + branch + " - Devices not checked in during last 48 hours"
    
    description = "Hi Team,\n"
    description += "The following devices have not checked in with the Ingenico TEM Portal during the last 48 hours:\n"
    description += asset_list
    description += "\n\nPlease call the Branch and ask if they are experiencing any issues with the devices.\n"
    description += "If there are issues, proceed with troubleshooting and escalate if required. If no apparent issues, please ask the Branch to reboot their devices and run a Test Transaction.\n"
    description += "Kind regards,\n"
    description += "GEMA Operations"
    
    body = "{"
    body = body + "\"validator\": \"create\","
    body = body + "\"client\": \"EHI\","
    body = body + "\"caller\": \"EHI L3K Check In Integration\","
    body = body + "\"ticketcat\": \"EHI: Lane 3K Counter Payment Device: Check-In Monitoring\","
    body = body + "\"priority\": \"P3\","
    #body = body + "\"email\": \"{}\",".format(email)
    body = body + "\"location\": \"{}\",".format(location)
    body = body + "\"title\": \"{}\",".format(title)
    body = body + "\"description\": \"{}\",".format(description)
    body = body + "\"phone\": \"{}\",".format(phone)
    body = body + "\"group\": \"{}\"".format(group)
    body = body + "}"
    json_body = json.loads(body, strict=False)

    try:
        #print(json.dumps(json_body, indent=4, sort_keys=True))
        endpoint = 'https://webservices-na01.vivantio.com/WebMethods/wMLane3KChkin'
        response = requests.post(
            endpoint, 
            data = body,
            auth = HTTPBasicAuth( vivantio_username, vivantio_password )
        )
        print(title, response)
    except Exception as e:
        print("ERROR: ", e)
    
def disable_notifications(df):
    # EXAMPLE: [Estate name, Location_Country, Serial number, Asset_AssetTag, Location_Phone, Last call end date, Location_Lada]
    assets = df['Asset_AssetTag']
    for asset in assets:
        dynamodb_response = dynamodb_client.put_item(
            TableName = "proyecto-ehi-last-report",
            Item = {
                "asset": {
                    "S": asset
                },
                "last_report": {
                    "S": str(today_date)
                }
            }
        )
        
def clean_report(dataframe):
    dataframe = dataframe.rename(
        columns = {
            "Asset_AssetTag": 'Asset_Tag',
            "Location_Country": 'Country',
            "Estate signature": "Estate_Signature",
            "Estate name": "Estate_Name", 
            "Part number": "Part_Number",
            "Terminal name": "Terminal_Name",
            "Terminal signature": "Terminal_Signature", 
            "Serial number": "Serial_Number", 
            "start_date": 'Last_Call_Start_Date',
            "end_date": 'Last_Call_End_Date',
            "status": 'Last_Call_Status',
            "Asset_CategoryLineage": "Asset_Category"
        }
    )
    #dataframe['Last_Call_Duration'] = pd.to_timedelta(dataframe['Last_Call_End_Date'] - dataframe['Last_Call_Start_Date'], unit = 's').astype(str).apply( lambda x: x[7:15] )
    #print(dataframe['Last_Call_Duration'])
    dataframe["Location_Phone"] = dataframe["Location_Phone"].str.replace(' ', '').str.replace('/', '').str.replace('+', '').str.replace('-', '').str.replace('(', '').str.replace(')', '')
    dataframe.loc[dataframe['Country'] == "FRA", 'Lada'] = "+33"
    dataframe.loc[dataframe['Country'] == "ES", 'Lada'] = "+34"
    dataframe.loc[dataframe['Country'] == "UK", 'Lada'] = "+44"
    dataframe.loc[dataframe['Country'] == "DE", 'Lada'] = "+49"
    dataframe.loc[dataframe['Country'] == "IE", 'Lada'] = "+353"
    dataframe = dataframe[[
        "Asset_Tag",
        "Country",
        "Estate_Signature",
        "Estate_Name", 
        "Location_Name",
        "Lada",
        "Location_Phone",
        "Part_Number",
        "Serial_Number",
        "Last_Call_End_Date", 
        "Last_Call_Status"
    ]]
    
    dataframe = write_status_report(dataframe)
    return dataframe
    
def write_status_report(dataframe):
    
    dynamodb_response = dynamodb_client.scan(
        TableName = "proyecto-ehi-last-report"
    )
    dynamodb_df = pd.DataFrame()
    json_subitems = {}
    for item in dynamodb_response['Items']:
        for subitem in item:
            json_subitems[subitem] = item[subitem]['S']
        dynamodb_df = dynamodb_df.append(json_subitems, ignore_index=True)
    cond = dataframe['Asset_Tag'].isin(dynamodb_df['asset'])
    dataframe.loc[
        (cond == True),
        'Last_Call_Status'
    ] = "DISABLED REPORTS"
    
    dataframe.loc[
        ( pd.to_datetime(dataframe["Last_Call_End_Date"]) >= str(limit_date) ),
        'Last_Call_Status'
    ] = "NO NEED CALL"
    
    dataframe.loc[
        (dataframe['Location_Phone'] == "") |
        (dataframe['Location_Phone'].isna()),
        'Last_Call_Status'
    ] = "NO PHONE"
    
    dataframe.loc[
        (dataframe["Estate_Name"] == "ESP") | 
        (dataframe["Estate_Name"] == "FRA") | 
        (dataframe["Estate_Name"] == "GER") |
        (dataframe["Estate_Name"] == "UK"),
        "Last_Call_Status"
    ] = "NOT ASSIGNED"
    
    dataframe.loc[
        (dataframe['Part_Number'] == "") |
        (dataframe['Part_Number'].isna()),
        'Last_Call_Status'
    ] = "NO ASSET DATA"
    
    dataframe.loc[
        (dataframe['Location_Name'] == "") |
        (dataframe['Location_Name'].isna()),
        'Last_Call_Status'
    ] = "TEST ASSET"
    
    dataframe.loc[
        (dataframe['Last_Call_Status'] == "") |
        (dataframe['Last_Call_Status'].isna()),
        'Last_Call_Status'
    ] = "REPORTING..."
    
    return dataframe
    
def analyze_dataframe(dataframe):
    results = "Reporte de assets del dia " + str(today_date) + " con fecha limite de llamada del " + str(limit_date) + "\n"
    results = results + "Hay un total de "+ str(dataframe.shape[0]) + " assets en el excel.\n\n"
    return results
    
def send_report_global(body_msg, filename):
    body = MIMEText(body_msg)
    msg = MIMEMultipart()
    msg["Subject"] = "Reporte general EHI Lane 3K " + today_date.strftime("%Y-%m-%d")
    msg["From"] = 'reports@thegema.com.mx'
    msg["To"] = "operations@thegema.com"
    msg.attach(body)
    
    with open('/tmp/' + filename, "rb") as attachment:
        part = MIMEApplication(attachment.read())
        part.add_header("Content-Disposition",
                        "attachment",
                        filename=filename)
    msg.attach(part)
    
    ses_response = ses_client_v1.send_raw_email(
        Source= 'reports@thegema.com.mx',
        Destinations=["operations@thegema.com"],
        RawMessage={"Data": msg.as_string()}
    )
    
def funcion_aux_dev():
    connect_response = connect_client.start_outbound_voice_contact(
        DestinationPhoneNumber = number, 
        ContactFlowId = "fc5094bb-502b-4af2-b4df-bf39c6dc3cb9",
        InstanceId = connect_instance,
        SourcePhoneNumber = "+525541637152",
        Attributes = {
            'asset01': '123456789',
            'asset02': '123456789'
        }
    )
    
    contact_id = connect_response["ContactId"]
    attr = connect_client.get_contact_attributes(
        InstanceId = "67c27dd4-cc8b-4873-b74d-0357f9dac3db",
        InitialContactId = contact_id
    )
    print("imprimiendo attributos")
    print(attr)