import json
import boto3
import datetime as dt

dynamodb_client = boto3.client('dynamodb')
dynamodb_table = 'proyecto-ehi-dynamodb'
today = dt.datetime.today()

def lambda_handler(event, context):
    print(event)
    assets = event['Details']['ContactData']['Attributes']

    last_call_end_date = today.strftime("%m-%d-%Y %H:%M:%S")
    print(last_call_end_date)
    
    for asset in assets:
        print(assets[asset])
        dynamodb_response = dynamodb_client.put_item(
            TableName = dynamodb_table,
            Item = {
                "asset": {
                    "S": assets[asset]
                },
                "date": {
                    "S": last_call_end_date
                },
                "status": {
                    "S": "SUCCESS"
                }
            }
        )
    
    return {
        'statusCode': 200
    }
